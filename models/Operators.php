<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "operators".
 *
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $email
 * @property string $birth_date
 * @property int $status_id
 * @property string $tel1
 * @property string $tel2
 * @property string $tel3
 * @property string $created_at
 */
class Operators extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'operators';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status_id'], 'integer'],
            [['created_at'], 'safe'],
            [['name', 'surname', 'email'], 'string', 'max' => 50],
            [['birth_date'], 'string', 'max' => 12],
            [['tel1', 'tel2', 'tel3'], 'string', 'max' => 20],
            [['name', 'surname', 'email', 'status_id'], 'required'],
            ['email', 'email'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Ім\'я оператора',
            'surname' => 'Прізвище оператора',
            'birth_date' => 'Дата народження (у форматі РРРР-ММ-ДД)',
            'email' => 'Email',
            'status_id' => 'Status ID',
            'tel1' => 'Придбаний номер телефону',
            'tel2' => 'Tel2',
            'tel3' => 'Tel3',
            'created_at' => 'Created At',
        ];
    }
}
