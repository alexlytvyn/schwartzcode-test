<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%operators}}`.
 */
class m190826_192414_create_operators_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('operators', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50),
            'surname' => $this->string(50),
            'email' => $this->string(50),
            'birth_date' => $this->string(12),
            'status_id' => $this->tinyInteger()->defaultValue(1),
            'tel1' => $this->string(20),
            'tel2' => $this->string(20),
            'tel3' => $this->string(20),
            'created_at' => $this->date('Y-m-d H:i:s')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('operators');
    }
}
