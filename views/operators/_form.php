<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Operators */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="operators-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
    <label for="countries_list">Тут ви можете придбати номер для оператора. Вкажіть країну для номера.</label>
    <?= Html::dropDownList('countries_list', null, ArrayHelper::map($countriesArray,'country_code','country'), ['prompt' => 'Вкажіть країну для придбання номера', 'class' => 'form-control', 
        'onchange' => '
            $.get("/operators/lists?id='.'"+$(this).val(), function(data) {
                $("select#phones_list").html(data);
            });
            ']); ?>
    </div>

    <div class="form-group">
    <label for="phones_list">Виберіть бажаний номер.</label>
    <?= Html::dropDownList('phones_list', null, ArrayHelper::map($phonesArray,'number','friendly_name'), ['prompt' => 'Виберіть номер після вибору країни', 'class' => 'form-control phones_list','id' => 'phones_list']); ?>
    <br>
    <?= Html::a('Придбати номер',['/operators/create'], ['class' => 'buy-number']);  ?>
    </div>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'birth_date')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status_id')->dropDownList(['1' => '1', '0' => '0']) ?>

    <?php     
        $this->registerJsFile("@web/js/script.js",[
            'depends' => [
                \yii\web\JqueryAsset::className()
            ]
        ]); 
    ?>

    <?= $form->field($model, 'tel1')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
