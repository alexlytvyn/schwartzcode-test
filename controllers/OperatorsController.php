<?php

namespace app\controllers;

use Yii;
use app\models\Operators;
use app\models\OperatorsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use Twilio\Rest\Client;

/**
 * OperatorsController implements the CRUD actions for Operators model.
 */
class OperatorsController extends Controller
{
    const SID = ''; // Тут вказується SID аккаунта Twilio
    const TOKEN = ''; // Тут вказується Authtoken аккаунта Twilio

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Operators models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OperatorsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /* Отримання списку доступиних номерів */
    public function actionLists($id)
    {
        $sid    = self::SID;
        $token  = self::TOKEN;
        $twilio = new Client($sid, $token);

        $mobile = $twilio->availablePhoneNumbers($id)
                 ->mobile
                 ->read(array(), 20);

        $phonesArray = [];

        foreach ($mobile as $record) {
            $arr_tmp = [];
            $arr_tmp['friendly_name'] = $record->friendlyName. ' '. '('. $record->isoCountry. ')';
            $arr_tmp['number'] = $record->phoneNumber;
            $phonesArray[] = $arr_tmp;
        }
        
        if (!empty($phonesArray)) {
            echo "<option value=''>Виберіть номер після вибору країни</option>";
            foreach ($phonesArray as $phone) {
                echo "<option value='". $phone['number']. "'>".$phone['friendly_name']."</option>";
            }
        } else {
            echo "<option>------</option>";
        }        
    }

    /* Купівля обраного номера */
    public function actionBuy($n) 
    {
        $sid    = self::SID;
        $token  = self::TOKEN;
        $twilio = new Client($sid, $token);

        $incoming_phone_number = $twilio->incomingPhoneNumbers
            ->create(array("phoneNumber" => $n)
        );

        print($incoming_phone_number->phoneNumber);
    }

    /**
     * Displays a single Operators model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Operators model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Operators();

        $sid    = self::SID;
        $token  = self::TOKEN;
        $twilio = new Client($sid, $token);

        // Отримання списку доступних країн

        $availablePhoneNumbers = $twilio->availablePhoneNumbers
                                ->read(array(), 200);
        $countriesArray = [];

        foreach ($availablePhoneNumbers as $record) {
            $arr_tmp = [];
            $arr_tmp['country_code'] = $record->countryCode;
            $arr_tmp['country'] = $record->country;
            $countriesArray[] = $arr_tmp;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'countriesArray' => $countriesArray,
            'phonesArray' => []
        ]);
        echo $sid;
    }

    /**
     * Updates an existing Operators model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Operators model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Operators model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Operators the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Operators::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
